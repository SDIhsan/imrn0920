// Looping while
console.log('LOOPING PERTAMA');
var x = 1;
while(x < 20)
{
    x = x + 1;
    console.log(x + ' - I love coding');
    x++;
}
console.log('LOOPING KEDUA');
var y = 20;
while(y > 1)
{
    console.log(y + ' - I will become a mobile developer');
    y = y - 1;
    y--;
}

console.log('');

// Looping menggunakan for
var max = 20;
for(var i = 1; i <= max; i++)
{
    if (i % 2 == 1){ // ganjil
        if (i % 3 == 0){ // kelipatan 3
            console.log(i + ' - I Love Coding');
        }else{
            console.log(i + ' - Santai');
        }
    }else if (i % 2 == 0){ // genap
        console.log(i + ' - Berkualitas');
    }
}

console.log('');

// Membuat Persegi Panjang #

for(var i = 0; i < 4; i++){
    var out = '';
    for(var j = 0; j < 8; j++){
        out += '#';
    }
    console.log(out);
}

console.log('');

// Membuat Tangga

var t = 0;
while (t < 7) {
    t++;
    var out = "#".repeat(t);
    console.log(out);
}

console.log('');

// Membuat Papan Catur
var h = 1;
while(h <= 8)
{
    var v = 1, out = '';
    while(v <= 8){ 
        if ((h + v) % 2 == 0) {
            out += '#';
        }else{
            out += ' ';
        }
        v++;
    }
    console.log(out);
    h++;
}