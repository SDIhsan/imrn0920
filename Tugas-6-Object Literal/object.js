//Array to Object

var now = new Date()
var thisYear = now.getFullYear() // 2020 (tahun sekarang)

arr = []
function arrayToObject(arr){
    for (i = 0; i < arr.length; i++){
        if (arr != null){
            if (arr[i][3] != null && arr[i][3] <= thisYear){
                a = thisYear - arr[i][3]
            }else{
                a = 'Invalid Birth Year'
            }
        }else{
            console.log("''");
        }
        var personObj = {
            firstName : arr[i][0],
            lastName : arr[i][1],
            gender : arr[i][2],
            age : a
        }
        console.log(i+1 + '. ' + personObj.firstName + ' ' + personObj.lastName + ': ');
        console.log(personObj);
    }
}
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""
var memberId = '';
var money = '';

function shoppingTime(memberId, money){
    var itemObj = ['Sepatu brand Stacattu','Baju brand Zoro','Baju brand H&N','Sweater brand Uniklooh','Casing Handphone'];
    var hargaObj = [1500000,500000,250000,175000,50000];
    var listObj = [];    
    var i = 0;
    var total = 0;
    if (memberId != null){
        if (money < 50000){
            console.log('Mohon maaf, uang tidak cukup');
        }else{
            
            // total = money;
            for (i = 0; i < itemObj.length; i++){
                if (money >= hargaObj[i]){
                    listObj.push(itemObj[i]);
                    total += hargaObj[i];
                }else{
                    break;
                }
                var shopObj = {
                    memberId: memberId,
                    money: money,
                    listPurchased: listObj,
                    changeMoney: money - total
                }
            }
            return shopObj;

        }
    } else {
        console.log('Mohon maaf, toko X hanya berlaku untuk member saja');
    }

}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
  //{ memberId: '1820RzKrnWn08',
  // money: 2475000,
  // listPurchased:
  //  [ 'Sepatu Stacattu',
  //    'Baju Zoro',
  //    'Baju H&N',
  //    'Sweater Uniklooh',
  //    'Casing Handphone' ],
  // changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

var arrPenumpang = [];
function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var ketObj = []
    if (arrPenumpang != null){
        while(i < arrPenumpang.length){
            var from = rute.indexOf(arrPenumpang[i][1])
            var to = rute.indexOf(arrPenumpang[i][2])
            var cash = 0
            if (from != 0 && to > 1){
                jln = to - from
                cash = jln * 2000
            }else if(to > 1){
                jln = to - from
                cash = jln * 2000
            }else{
                cash = 2000
            }
            var ket = {
                penumpang: arrPenumpang[i][0],
                naikDari: arrPenumpang[i][1],
                tujuan: arrPenumpang[i][2],
                bayar: cash
            }
            ketObj.push(ket);
            i++;
        }
        return ket;

    }
  }
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]