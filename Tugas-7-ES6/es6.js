// Mengubah fungsi menjadi fungsi arrow

// const golden = function goldenFunction(){
//     console.log("this is golden!!")
// }

// golden()

// ES6
// const golden = goldenFunction = () => {
const golden = () => {
    console.log("this is golden!!")
}

golden()

// Sederhanakan menjadi Object literal di ES6

// const newFunction = function literal(firstName, lastName){
//     return {
//       firstName: firstName,
//       lastName: lastName,
//       fullName: function(){
//         console.log(firstName + " " + lastName)
//         return 
//     }
//     }
// }

// ES6
// const newFunction = literal = (firstName, lastName) => {
const newFunction = (firstName, lastName) => {
    return {
        firstName, 
        lastName,
        fullName() {
            console.log(`${firstName} ${lastName}`)
            // return
        }
        // fullName() {
        //     console.log(firstName, lastName)
        //     return
        // }
    }
}
//Driver Code 
newFunction("William", "Imoh").fullName() 

// Destructuring

// const newObject = {
//     firstName: "Harry",
//     lastName: "Potter Holt",
//     destination: "Hogwarts React Conf",
//     occupation: "Deve-wizard Avocado",
//     spell: "Vimulus Renderus!!!"
//   }
//   const firstName = newObject.firstName;
//   const lastName = newObject.lastName;
//   const destination = newObject.destination;
//   const occupation = newObject.occupation;

// ES6
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
};
const { firstName, lastName, destination, occupation, spell } = newObject; 
// Driver code
console.log(firstName, lastName, destination, occupation)

// Array Spreading

// const west = ["Will", "Chris", "Sam", "Holly"]
// const east = ["Gill", "Brian", "Noel", "Maggie"]
// const combined = west.concat(east)

// ES6
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
//Driver Code
console.log(combined)

// Template Literals

// const planet = "earth"
// const view = "glass"
// var before = 'Lorem ' + view + ' dolor sit amet, ' +  
//     'consectetur adipiscing elit,' + planet + ' do eiusmod tempor ' +
//     'incididunt ut labore et dolore magna aliqua. Ut enim' +
//     ' ad minim veniam'

// ES6
const planet = "earth"
const view = "glass"
var before = `Lorem ${view} dolor sit amet,  consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
// Driver Code
console.log(before) 