import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, TouchableOpacity, FlatList, TextInput, Button } from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
// import { Button } from 'react-native-elements';


export default class LoginScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            style: {},
        };
      }
    onFocus = () => {
        const state = { ...this.state };
        state.style = {
          borderStyle: 'solid',
          borderColor: '#3EC6FF',
        };    
        this.setState(state);
      }    
      onBlur = () => {
        const state = { ...this.state };
        state.style = {};
    
        this.setState(state);
      }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.logo}>
                    <Image source={require('./images/logo.png')} />
                </View>
                <View style={styles.body}>
                    <Text style={styles.loginText}>Login</Text>
                        <View style={[styles.userInput, this.state.style]} onFocus={() => this.onFocus()} onBlur={() => this.onBlur()}>
                            <TextInput style={styles.inputText} placeholder="Username/Email" placeholderTextColor='#003366'>
                            </TextInput>
                        </View>
                        <View style={styles.passInput}>
                            <TextInput style={styles.inputText} placeholder="Password" textContentType="password" placeholderTextColor='#003366' secureTextEntry></TextInput>
                        </View>
                        <TouchableOpacity style={styles.loginButton}>                                         
                            <Text style={styles.tabTitleLogin}><Icon name="login" size={25} />  Login</Text>
                        </TouchableOpacity>
                        <View style={styles.or}>
                            <Text style={{textAlign: 'center'}}>OR</Text>
                        </View>
                        <TouchableOpacity style={styles.signupButton}>                                         
                            <Text style={styles.tabTitleSignup}><Icon name="adduser" size={25} />  Sign Up</Text>
                        </TouchableOpacity>
                </View>
            </View>
        );
    };
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // position: 'relative',
        // backgroundColor: '#FFF'
    },
    logo: {
        position: 'absolute',
        flex: 1,
        width: 375,
        height: 102,
        left: 15,
        // right: 0,
        top: 63,
        alignItems: 'center',
        flexDirection: 'row',
    },
    body: {
        flex: 1,
        // elevation: 3,
        // flexDirection: 'row',
        alignItems: 'center',
        // backgroundColor: 'white'
    },
    loginText: {
        // position: 'absolute',        
        top: 235,        
        width: 60,
        height: 28,
        // left: 180,
        // alignItems: 'center',
        textAlign: 'center',
        fontFamily: 'Roboto',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 24,
        lineHeight: 28,
        color: '#003366'
    },
    userInput:{
        position: 'absolute', 
        width: 294,
        height: 48,
        // left: 41,
        top: 306,

        backgroundColor: '#FFFFFF',        
        borderColor: '#003366',
        borderWidth: 1,  
        borderRadius: 25,    
        marginBottom: 20,    
        alignItems: 'center',    
        padding:10
    },
    passInput:{
        position: 'absolute', 
        width: 294,
        height: 48,
        // left: 41,
        top: 386,

        backgroundColor: '#FFFFFF',        
        borderColor: '#003366',
        borderWidth: 1,  
        borderRadius: 25,    
        marginBottom: 20,    
        alignItems: 'center',    
        padding:10
    },
    inputText:{
        // alignItems: 'flex-start',
        textAlign: 'center'
        // height: 50,
    },
    loginButton: {
        position: 'absolute',
        width: 140,
        height: 40,
        // left: 118,
        top: 473,
        alignItems: 'center',
        borderRadius: 25,
        backgroundColor: '#3EC6FF',
        justifyContent: 'space-between',
        // paddingTop: 3

        
    },
    or: {
        flex: 1
    },
    signupButton: {
        position: 'absolute',
        width: 140,
        height: 40,
        // left: 118,
        top: 563,
        alignItems: 'center',
        borderRadius: 25,
        backgroundColor: '#003366',
        justifyContent: 'space-between',
        color: 'white'
        // paddingTop: 3

        
    },
    tabTitleLogin: {
        fontSize: 24,
        color: '#3c3c3c',
        alignItems: 'center',
        paddingTop: 6
    },
    tabTitleSignup: {
        fontSize: 24,
        color: '#FFF',
        alignItems: 'center',
        paddingTop: 6
    }

});
