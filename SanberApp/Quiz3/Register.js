import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, TouchableOpacity, FlatList, TextInput, Button } from 'react-native';
// import { NavigationContainer } from '../node_modules/@react-navigation/native';
// import { createStackNavigator } from '../node_modules/@react-navigation/stack';
import Icon from 'react-native-vector-icons/MaterialIcons';


export default class LoginScreen extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <View>
                    <Text style={styles.welcomeText}>Welcome</Text>
                    <Text style={styles.signinText}>Sign up to continue</Text>
                </View>
                <View style={styles.rectangle}>
                    <View style={{margin: 20}}>
                        <View style={styles.nameInput}>
                            <Text>Name</Text>
                            <TextInput style={styles.inputText} placeholderTextColor='#003366'>
                            </TextInput>
                        </View>
                        <View style={styles.emailInput}>
                            <Text>Email</Text>
                            <TextInput style={styles.inputText} placeholderTextColor='#003366'>
                            </TextInput>
                        </View>
                        <View style={styles.noInput}>
                            <Text>Phone Number</Text>
                            <TextInput style={styles.inputText} placeholderTextColor='#003366'>
                            </TextInput>
                        </View>
                        <View style={styles.passInput}>
                            <Text>Password</Text>
                            <TextInput style={styles.inputText} textContentType="password" placeholderTextColor='#003366' secureTextEntry></TextInput>
                        </View>
                        <TouchableOpacity style={styles.signUpButton}>                                         
                            <Text style={styles.tabTitleSignUp}>Sign Up</Text>
                        </TouchableOpacity>
                        <View style={{flex:1, flexDirection: 'row', top:390, alignSelf: 'center', fontSize: 12}}>
                                <Text style={{width:115, height:15}}>Already has account? </Text>
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('signin')}>
                                    <Text style={styles.link}>Sign in</Text>
                                </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </View>
        );
    };
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    welcomeText: {
        position: 'absolute',
        height: 37,
        width: 237,
        top: 153,
        left: 24,
        right: 153,
        // fontFamily: 'Montserrat',
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: 30,
        lineHeight: 37,

        color: '#0C0423',
    },
    signinText: {
        position: 'absolute',
        top: 190,
        left: 27,
        // fontFamily: 'Montserrat',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 12,
        lineHeight: 15,

        color: '#4D4D4D',
    },
    rectangle: {
        position: 'absolute',
        left: 24,
        width: 366,
        height: 486,
        top: 239,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowColor: "#000000",
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        backgroundColor: '#FFFFFF',
        borderRadius: 11,
    },
    nameInput:{
        position: 'absolute', 
        width: 326,
        height: 48,
        top: 20,

        backgroundColor: '#FFFFFF',   
        marginBottom: 20,    
        alignItems: 'flex-start', 
        borderBottomWidth: 1,   
    },
    emailInput:{
        position: 'absolute', 
        width: 326,
        height: 48,
        top: 100,

        backgroundColor: '#FFFFFF',   
        marginBottom: 20,    
        alignItems: 'flex-start', 
        borderBottomWidth: 1,   
    },
    noInput:{
        position: 'absolute', 
        width: 326,
        height: 48,
        top: 180,

        backgroundColor: '#FFFFFF',   
        marginBottom: 20,    
        alignItems: 'flex-start', 
        borderBottomWidth: 1,   
    },
    passInput:{
        position: 'absolute', 
        width: 326,
        height: 48,
        top: 260,

        backgroundColor: '#FFFFFF',   
        marginBottom: 20,    
        alignItems: 'flex-start', 
        borderBottomWidth: 1,
    },
    inputText:{
        textAlign: 'center'
    },
    signUpButton: {
        position: 'absolute',
        width: 326,
        height: 40,
        // left: 118,
        top: 340,
        alignItems: 'center',
        borderRadius: 6,
        backgroundColor: '#F77866',
        justifyContent: 'space-between',
        // paddingTop: 3

        
    },
    link: {
        width: 43,
        height: 15,
        color: 'blue',
    },
    tabTitleSignUp: {
        fontSize: 24,
        color: '#FFF',
        alignItems: 'center',
        paddingTop: 6
    }

});
