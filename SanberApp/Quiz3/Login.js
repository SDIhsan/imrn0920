import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, TouchableOpacity, FlatList, TextInput, Button } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';


export default class LoginScreen extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <View>
                    <Text style={styles.welcomeText}>Welcome Back</Text>
                    <Text style={styles.signinText}>Sign in to continue</Text>
                </View>
                <View style={styles.rectangle}>
                    <View style={{margin: 20}}>
                        <View style={styles.emailInput}>
                            <Text>Email</Text>
                            <TextInput style={styles.inputText} placeholderTextColor='#003366'>
                            </TextInput>
                        </View>
                        <View style={styles.passInput}>
                            <Text>Password</Text>
                            <TextInput style={styles.inputText} textContentType="password" placeholderTextColor='#003366' secureTextEntry></TextInput>
                        </View>
                        <View style={{alignItems: 'flex-end', top: 170}}>
                            <TouchableOpacity>
                                <Text>Forgot Password?</Text>
                            </TouchableOpacity>
                        </View>
                        <TouchableOpacity style={styles.signInButton}>                                         
                            <Text style={styles.tabTitleSignIn}>Sign In</Text>
                        </TouchableOpacity>
                        <View style={{alignItems: 'center', top: 280}}>
                            <Text>-OR-</Text>
                        </View>
                        <View style={{flex: 1, flexDirection: 'row', alignSelf: 'center', top: 310}}>
                                <TouchableOpacity style={{right: 13}}>
                                    <Image source={require('./images/facebook.png')}/>
                                </TouchableOpacity>
                                <TouchableOpacity style={{left: 13}}>
                                    <Image source={require('./images/Google.png')}/>
                                </TouchableOpacity>
                        </View>
                    </View>
                </View>
                {/* <View style={{position: 'absolute', bottom:0,}}>
                    <Image source={require('./images/Home_Indicator.png')}/>
                </View> */}
            </View>
        );
    };
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    welcomeText: {
        position: 'absolute',
        height: 37,
        width: 237,
        top: 153,
        left: 24,
        right: 153,
        // fontFamily: 'Montserrat',
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: 30,
        lineHeight: 37,

        color: '#0C0423',
    },
    signinText: {
        top: 197,
        left: 27,
        // fontFamily: 'Montserrat',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 12,
        lineHeight: 15,

        color: '#4D4D4D',
    },
    rectangle: {
        position: 'absolute',
        left: 24,
        width: 366,
        height: 456,
        top: 239,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowColor: "#000000",
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        backgroundColor: '#FFFFFF',
        borderRadius: 11,
    },
    emailInput:{
        position: 'absolute', 
        width: 326,
        height: 48,
        top: 20,

        backgroundColor: '#FFFFFF',   
        marginBottom: 20,    
        alignItems: 'flex-start', 
        borderBottomWidth: 1,   
    },
    passInput:{
        position: 'absolute', 
        width: 326,
        height: 48,
        top: 100,

        backgroundColor: '#FFFFFF',   
        marginBottom: 20,    
        alignItems: 'flex-start', 
        borderBottomWidth: 1,
    },
    inputText:{
        textAlign: 'center'
    },
    signInButton: {
        position: 'absolute',
        width: 326,
        height: 40,
        // left: 118,
        top: 230,
        alignItems: 'center',
        borderRadius: 6,
        backgroundColor: '#F77866',
        justifyContent: 'space-between',
        // paddingTop: 3

        
    },
    link: {
        width: 43,
        height: 15,
        color: 'blue',
    },
    tabTitleSignIn: {
        fontSize: 24,
        color: '#FFF',
        alignItems: 'center',
        paddingTop: 6
    }
});
