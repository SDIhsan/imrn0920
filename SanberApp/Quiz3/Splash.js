import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, TouchableOpacity, FlatList, TextInput, Button, ImageBackground } from 'react-native';
// import Icon from 'react-native-vector-icons/AntDesign';
import Icon from 'react-native-vector-icons/MaterialIcons';


export default class Splash extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                    <View style={styles.circleShape}>
                    </View>
                        <Image source={require('./images/logo.png')} style={{width:223, height:133, top: 365, left: 95,}} />
            </View>
        );
    };
};

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    circleShape: {
        position: 'absolute',
        width: 300,
        height: 300,
        top: 279,
        left: 58,
        opacity: 0.1,
        borderRadius: 150,
        backgroundColor: '#211F65',
        alignItems: 'center',
        
    },
});
