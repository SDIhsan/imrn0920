// Range

function range(num1, num2) {
    var arraynum = [];
    if (num1 <= num2){
        for (let i = num1; i <= num2; i++){
            arraynum.push(i);
        }
    }else if (num1 >= num2){
        for (let i = num1; i >= num2; i--){
            arraynum.push(i);
        }
    }else{
        arraynum.push(-1);
    }
    return arraynum;
}
// console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
// console.log(range(1)) // -1
// console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
// console.log(range(54, 50)) // [54, 53, 52, 51, 50]
// console.log(range()); // -1

function rangeWithStep(num1, num2, step){
    var arrayStepnum = [];
    if (num1 <= num2){
        for (var i = num1; i <= num2; i+=step){
            arrayStepnum.push(i);
        }
    }else if (num1 >= num2){
        for (var i = num1; i >= num2; i-=step){
            arrayStepnum.push(i);
        }
    }else{
        arrayStepnum.push(-1);
    }
    return arrayStepnum;
}

// console.log(rangeWithStep(1, 10, 2)); // [1, 3, 5, 7, 9]
// console.log(rangeWithStep(11, 23, 3)); // [11, 14, 17, 20, 23]
// console.log(rangeWithStep(5, 2, 1)); // [5, 4, 3, 2]
// console.log(rangeWithStep(29, 2, 4)); // [29, 25, 21, 17, 13, 9, 5]

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]
function dataHandling(){
    for (i = 0; i < input.length; i++){
        console.log('Nomor ID : ' + input[i][0])
        console.log('Nama Lengkap : ' + input[i][1])
        console.log('TTL : ' + input[i][2] + ' ' + input[i][3])
        console.log('Hobi : ' + input[i][4])
        console.log('')
    }
}
console.log(dataHandling())

