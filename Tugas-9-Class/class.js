// Animal class

// Release 0
class Animal {
    constructor(name) {
        this._name = name
        this.legs = 4
        this.cold_blooded = false
    }
    get name() {
        return this._name
    }
}

var sheep = new Animal("shaun");

console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

console.log("")
// Release 1
class Ape extends Animal {
    constructor(name) {
        super(name)
        this.legs = 2
        this._yell = 'Auooo'
    }
    get yell() {
        return this._yell
    }
    yell() {
        console.log(this._yell)
    }
}
class Frog extends Animal {
    constructor(name) {
        super(name)
        this._jump = 'hop hop'
    }
    get jump() {
        return this._jump
    }
    jump() {
        console.log(this._jump)
    }
}

var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
// console.log(sungokong.legs)

var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 

console.log("")
// Function to Class
class Clock {
    constructor({template}) {
        this.template = template   
    }
    render() {
        let date = new Date()
        let hours = date.getHours()
        if (hours < 10) hours = '0' + hours;

        let mins = date.getMinutes()
        if (mins < 10) mins = '0' + mins

        let secs = date.getSeconds()
        if (secs < 10) secs = '0' + secs

        let output = this.template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);

        console.log(output)
    }
    stop() {
        clearInterval(this.timer)
    }
    start() {
        this.render()
        this.timer = setInterval(() => this.render(), 1000)
    }

}

var clock = new Clock({template: 'h:m:s'});
clock.start(); 